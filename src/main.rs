// Copyright (c) 2016 gosh developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! gosh - goopy SSH client
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy))]
#![deny(missing_docs)]
#![feature(try_from)]
#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate slog;

extern crate bytes;
extern crate dns_lookup;
extern crate goopy;
extern crate mio;
extern crate net2;
extern crate slog_json;
extern crate slog_term;
extern crate regex;
extern crate rustc_serialize;
extern crate toml;

mod client;
mod config;
mod error;
mod state;

use clap::{App, Arg};
use client::Gosh;
use config::{GoshToml, Ipv4ConnectionToml, Ipv6ConnectionToml};
use error::GoshErr;
use mio::EventLoop;
use mio::tcp::TcpStream;
use net2::TcpBuilder;
use slog::Level;
use slog::drain::{self, AtomicSwitchCtrl, IntoLogger};
use std::fs::OpenOptions;
use std::io;
use std::net::{SocketAddr, SocketAddrV4, SocketAddrV6};
use std::process;
use std::sync::mpsc::channel;
use std::thread;

/// Gosh Version
pub const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
/// Gosh Package Name
pub const PKG: Option<&'static str> = option_env!("CARGO_PKG_NAME");

lazy_static! {
    /// stdout Drain switch
    pub static ref STDOUT_SW: AtomicSwitchCtrl = AtomicSwitchCtrl::new(
        drain::filter_level(
            Level::Error,
            drain::async_stream(io::stdout(), slog_term::format_colored())
        )
    );
    /// stderr Drain switch
    pub static ref STDERR_SW: AtomicSwitchCtrl = AtomicSwitchCtrl::new(
        drain::async_stream(io::stderr(), slog_term::format_colored())
    );
}

/// Result used in gosh.
pub type GoshResult<T> = Result<T, GoshErr>;

fn config_ivp4_stream(config: &Ipv4ConnectionToml) -> GoshResult<TcpStream> {
    let stdout = STDOUT_SW.drain().into_logger(o!());
    let address = try!(config.address().unwrap_or(&"127.0.0.1".to_string()).parse());
    let port = config.port().unwrap_or(2222);
    info!(stdout, "event_loop", "ipv4" => format!("{}", address), "port" => port);
    let addr = SocketAddr::V4(SocketAddrV4::new(address, port));
    let tcp = try!(TcpBuilder::new_v4());
    let stream = try!(tcp.to_tcp_stream());
    Ok(try!(TcpStream::connect_stream(stream, &addr)))
}

fn config_ipv6_stream(config: &Ipv6ConnectionToml) -> GoshResult<TcpStream> {
    let stdout = STDOUT_SW.drain().into_logger(o!());
    let address = try!(config.address().unwrap_or(&"::1".to_string()).parse());
    let port = config.port().unwrap_or(2222);
    let flowinfo = config.flowinfo().unwrap_or(0);
    let scope_id = config.scope_id().unwrap_or(32);
    info!(
        stdout,
        "event_loop",
        "ipv6" => format!("{}", address),
        "port" => port,
        "flowinfo" => flowinfo,
        "scope_id" => scope_id
    );
    let addr = SocketAddr::V6(SocketAddrV6::new(address, port, flowinfo, scope_id));
    let tcp = try!(TcpBuilder::new_v6());
    let stream = try!(tcp.to_tcp_stream());
    Ok(try!(TcpStream::connect_stream(stream, &addr)))
}

fn event_loop(config: GoshToml) -> GoshResult<()> {
    let stream = if let Some(conn_cfg) = config.ipv4() {
        try!(config_ivp4_stream(conn_cfg))
    } else if let Some(conn_cfg) = config.ipv6() {
        try!(config_ipv6_stream(conn_cfg))
    } else {
        return Err(GoshErr::Config);
    };

    let mut event_loop = try!(EventLoop::new());
    let (tx, rx) = channel();
    let mut client = try!(Gosh::new(stream, config));
    client.register(&mut event_loop);

    thread::spawn(move || {
        let stderr = STDERR_SW.drain().into_logger(o!());
        match event_loop.run(&mut client) {
            Ok(_) => {}
            Err(e) => {
                error!(
                    stderr,
                    "event_loop",
                    "error" => "error running event_loop",
                    "detail" => format!("{}", e)
                );
            }
        }

        tx.send(0).expect("Unable to send to channel");
    });

    rx.recv().expect("Unable to receive on channel");
    Ok(())
}

fn run(opt_args: Option<Vec<&str>>) -> i32 {
    let app = App::new("gosh")
        .version(crate_version!())
        .author("Jason Ozias <jason.g.ozias@gmail.com>")
        .about("goopy ssh client")
        .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .value_name("CONFIG")
            .help("Specify a non-standard path for the config file.")
            .takes_value(true))
        .arg(Arg::with_name("ipv4")
            .short("4")
            .long("ipv4")
            .help("The address specified is an IPV4 address (this is the default)."))
        .arg(Arg::with_name("ipv6")
            .short("6")
            .long("ipv6")
            .help("The address specified is an IPV6 address."))
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .value_name("IP_PORT")
            .help("Specify the port that the client should connect to.")
            .takes_value(true))
        .arg(Arg::with_name("dry_run")
            .long("dryrun")
            .help("Parse config and setup the client, but don't run it."))
        .arg(Arg::with_name("verbose")
            .short("v")
            .multiple(true)
            .help("Set the output verbosity level (more v's = more verbose)"))
        .arg(Arg::with_name("json")
            .short("j")
            .long("json")
            .help("Enable json logging at the given path")
            .value_name("PATH")
            .takes_value(true))
        .arg(Arg::with_name("address")
            .value_name("IP_ADDR")
            .help("The IPV4/6 addres that the client should connect to.")
            .index(1)
            .required(true));

    let matches = if let Some(args) = opt_args {
        app.get_matches_from(args)
    } else {
        app.get_matches()
    };

    // Setup the logging
    let level = match matches.occurrences_of("verbose") {
        0 => Level::Error,
        1 => Level::Warning,
        2 => Level::Info,
        3 => Level::Debug,
        4 | _ => Level::Trace,
    };

    let mut json_drain = None;
    if let Some(json_path) = matches.value_of("json") {
        if let Ok(json_file) = OpenOptions::new().create(true).append(true).open(json_path) {
            json_drain = Some(drain::stream(json_file, slog_json::new()));
        }
    }

    let stdout_base = drain::async_stream(io::stdout(), slog_term::format_colored());
    if let Some(json) = json_drain {
        STDOUT_SW.set(drain::filter_level(level, drain::duplicate(stdout_base, json)));
    } else {
        STDOUT_SW.set(drain::filter_level(level, stdout_base));
    }

    if matches.is_present("dry_run") {
        let stdout = STDOUT_SW.drain().into_logger(o!());
        warn!(stdout, "run", "message" => "Not starting event loop!", "dryrun" => "true");
        0
    } else if let Err(e) = event_loop(GoshToml::new(matches)) {
        let stderr = STDERR_SW.drain().into_logger(o!());
        error!(stderr, "run", "error" => "error running event_loops", "detail" => format!("{}", e));
        1
    } else {
        0
    }
}

fn main() {
    process::exit(run(None));
}

#[cfg(test)]
mod main_test {

    use super::run;

    #[test]
    fn command_line() {
        assert!(0 == run(Some(vec!["gosh", "-vvvv", "--dryrun", "localhost"])));
        assert!(0 == run(Some(vec!["gosh", "--dryrun", "-c", "test_cfg/gosh.toml", "localhost"])))
    }
}
