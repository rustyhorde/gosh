use goopy::error::GoopyErr;
use std::error::Error;
use std::{ffi, fmt, io, net, num};

#[derive(Debug)]
pub enum GoshErr {
    Config,
    Goopy(GoopyErr),
    InvalidKexPayload,
    InvalidReadableState,
    InvalidVersionExchange,
    Io(io::Error),
    Nul(ffi::NulError),
    ParseAddr(net::AddrParseError),
    ParseInt(num::ParseIntError),
}

impl fmt::Display for GoshErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            GoshErr::Config => write!(f, "Error parsing config toml"),
            GoshErr::Goopy(ref e) => write!(f, "{}", e),
            GoshErr::InvalidKexPayload => write!(f, "Invalid key exchange payload"),
            GoshErr::InvalidReadableState => write!(f, "Invalid readable state"),
            GoshErr::InvalidVersionExchange => write!(f, "Invalid version exchange"),
            GoshErr::Io(ref e) => write!(f, "{}", e),
            GoshErr::Nul(ref e) => write!(f, "{}", e),
            GoshErr::ParseAddr(ref e) => write!(f, "{}", e),
            GoshErr::ParseInt(ref e) => write!(f, "{}", e),
        }
    }
}

impl Error for GoshErr {
    fn description(&self) -> &str {
        match *self {
            GoshErr::Config => "Error parsing config toml",
            GoshErr::Goopy(ref e) => e.description(),
            GoshErr::InvalidKexPayload => "Invalid key exchange payload",
            GoshErr::InvalidReadableState => "Invalid readable state",
            GoshErr::InvalidVersionExchange => "Invalid version exchange",
            GoshErr::Io(ref e) => e.description(),
            GoshErr::Nul(ref e) => e.description(),
            GoshErr::ParseAddr(ref e) => e.description(),
            GoshErr::ParseInt(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            GoshErr::Config |
            GoshErr::InvalidKexPayload |
            GoshErr::InvalidReadableState |
            GoshErr::InvalidVersionExchange => None,
            GoshErr::Goopy(ref e) => Some(e),
            GoshErr::Io(ref e) => Some(e),
            GoshErr::Nul(ref e) => Some(e),
            GoshErr::ParseAddr(ref e) => Some(e),
            GoshErr::ParseInt(ref e) => Some(e),
        }
    }
}

impl From<io::Error> for GoshErr {
    fn from(e: io::Error) -> GoshErr {
        GoshErr::Io(e)
    }
}

impl From<net::AddrParseError> for GoshErr {
    fn from(e: net::AddrParseError) -> GoshErr {
        GoshErr::ParseAddr(e)
    }
}

impl From<GoopyErr> for GoshErr {
    fn from(e: GoopyErr) -> GoshErr {
        GoshErr::Goopy(e)
    }
}
