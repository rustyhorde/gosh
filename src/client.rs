use {GoshResult, PKG, STDERR_SW, STDOUT_SW, VERSION};
use bytes::{Buf, MutBuf, RingBuf};
use config::GoshToml;
use error::GoshErr;
use goopy::message::{Payload, SshPayload, kexinit};
use goopy::packet;
use goopy::types::{CompressionAlgorithm, EncryptionAlgorithm, KeyExchangeAlgorithm, MacAlgorithm,
                   ServerHostKeyAlgorithm, SshId, VER_EXCH_PROTO, VersionExchange};
use goopy::utils::buf::{TryRead, TryWrite};
use mio::{EventLoop, EventSet, Handler, PollOpt, Token};
use mio::tcp::TcpStream;
use slog::Logger;
use slog::drain::IntoLogger;
use state;
use state::ConnectionEvent::{ServerKeyExchangeReceived, ServerVersionExchangeReceived};
use std::convert::TryFrom;

pub struct Gosh {
    config: GoshToml,
    id: SshId,
    interest: EventSet,
    rx: RingBuf,
    sock: TcpStream,
    state: state::Connection,
    stderr: Logger,
    stdout: Logger,
    token: Token,
    tx: RingBuf,
    version_exchange: VersionExchange,
}

impl Gosh {
    pub fn new(sock: TcpStream, config: GoshToml) -> GoshResult<Gosh> {
        if let Ok(id) = SshId::new(PKG.unwrap_or("gosh"), VERSION.unwrap_or("unk"), None) {
            let mut version_exchange: VersionExchange = Default::default();
            version_exchange.set_client_version(id.id().to_string());
            Ok(Gosh {
                config: config,
                id: id,
                interest: EventSet::none(),
                rx: RingBuf::new(2048),
                sock: sock,
                state: state::Connection::VersionExchange,
                stderr: STDERR_SW.drain().into_logger(o!("struct" => "Gosh")),
                stdout: STDOUT_SW.drain().into_logger(o!("struct" => "Gosh")),
                token: Token(1),
                tx: RingBuf::new(2048),
                version_exchange: version_exchange,
            })
        } else {
            Err(GoshErr::Config)
        }
    }

    pub fn id(&self) -> &str {
        self.id.id()
    }

    /// Register ourselves with the event loop
    pub fn register(&mut self, event_loop: &mut EventLoop<Gosh>) {
        if let Err(e) = event_loop.register(&self.sock,
                                            self.token,
                                            EventSet::readable(),
                                            PollOpt::edge() | PollOpt::oneshot()) {
            {
                // Nested to drop immutable reference to self.token.
                error!(
                    self.stderr,
                    "register",
                    "error" => "Failed to register client token",
                    "token" => self.token.0,
                    "detail" => format!("{}", e)
                );
            }
            let client_token = self.token;
            self.reset_connection(event_loop, client_token);
        }
    }

    fn add_kex_algos(&self, kex_algos_vec: &[String], kim: &mut kexinit::Message) {
        let mut new_algos = Vec::new();
        for kex_algo_str in kex_algos_vec {
            match KeyExchangeAlgorithm::try_from(kex_algo_str) {
                Ok(kex_algo) => {
                    new_algos.push(kex_algo);
                    trace!(self.stdout, "add_kex_algos", "kex_algo" => format!("{}", kex_algo));
                }
                Err(e) => {
                    error!(self.stderr, "add_kex_algos", "error" => format!("{}", e));
                }
            }
        }

        if !new_algos.is_empty() {
            kim.set_kex_algos(new_algos);
        }
    }

    fn add_shk_algos(&self, shk_algos_vec: &[String], kim: &mut kexinit::Message) {
        let mut new_algos = Vec::new();
        for shk_algo_str in shk_algos_vec {
            match ServerHostKeyAlgorithm::try_from(shk_algo_str) {
                Ok(shk_algo) => {
                    new_algos.push(shk_algo);
                    trace!(self.stdout, "add_shk_algos", "shk_algo" => format!("{}", shk_algo));
                }
                Err(e) => {
                    error!(self.stderr, "add_shk_algos", "error" => format!("{}", e));
                }
            }
        }

        if !new_algos.is_empty() {
            kim.set_shk_algos(new_algos);
        }
    }

    fn add_enc_algos(&self, enc_algos_vec: &[String], kim: &mut kexinit::Message, c2s: bool) {
        let mut new_algos = Vec::new();
        for enc_algo_str in enc_algos_vec {
            match EncryptionAlgorithm::try_from(enc_algo_str) {
                Ok(enc_algo) => {
                    new_algos.push(enc_algo);
                    trace!(
                        self.stdout,
                        "add_enc_algos",
                        "enc_algo" => format!("{}", enc_algo),
                        "c2s" => c2s
                    );
                }
                Err(e) => {
                    error!(self.stderr, "add_enc_algos", "error" => format!("{}", e));
                }
            }
        }

        if !new_algos.is_empty() {
            if c2s {
                kim.enc_algos_c2s(new_algos);
            } else {
                kim.enc_algos_s2c(new_algos);
            }
        }
    }

    fn add_mac_algos(&self, mac_algos_vec: &[String], kim: &mut kexinit::Message, c2s: bool) {
        let mut new_algos = Vec::new();
        for mac_algo_str in mac_algos_vec {
            match MacAlgorithm::try_from(mac_algo_str) {
                Ok(mac_algo) => {
                    new_algos.push(mac_algo);
                    trace!(
                        self.stdout,
                        "add_mac_algos",
                        "mac_algo" => format!("{}", mac_algo),
                        "c2s" => c2s
                    );
                }
                Err(e) => {
                    error!(self.stderr, "add_mac_algos", "error" => format!("{}", e));
                }
            }
        }

        if !new_algos.is_empty() {
            if c2s {
                kim.mac_algos_c2s(new_algos);
            } else {
                kim.mac_algos_s2c(new_algos);
            }
        }
    }

    fn add_comp_algos(&self, comp_algos_vec: &[String], kim: &mut kexinit::Message, c2s: bool) {
        let mut new_algos = Vec::new();
        for comp_algo_str in comp_algos_vec {
            match CompressionAlgorithm::try_from(comp_algo_str) {
                Ok(comp_algo) => {
                    new_algos.push(comp_algo);
                    trace!(
                        self.stdout,
                        "add_comp_algos",
                        "comp_algo" => format!("{}", comp_algo),
                        "c2s" => c2s
                    );
                }
                Err(e) => {
                    error!(self.stderr, "add_comp_algos", "error" => format!("{}", e));
                }
            }
        }

        if !new_algos.is_empty() {
            if c2s {
                kim.comp_algos_c2s(new_algos);
            } else {
                kim.comp_algos_s2c(new_algos);
            }
        }
    }

    fn build_kex_message(&self) -> kexinit::Message {
        let mut kim: kexinit::Message = Default::default();

        if let Some(kim_cfg) = self.config.kex_init_message() {
            if let Some(kex_algos_vec) = kim_cfg.kex_algorithms() {
                self.add_kex_algos(kex_algos_vec, &mut kim);
            }

            if let Some(shk_algos_vec) = kim_cfg.shk_algorithms() {
                self.add_shk_algos(shk_algos_vec, &mut kim);
            }

            if let Some(enc_algos_c2s_vec) = kim_cfg.enc_algos_c2s() {
                self.add_enc_algos(enc_algos_c2s_vec, &mut kim, true);
            }

            if let Some(enc_algos_s2c_vec) = kim_cfg.enc_algos_s2c() {
                self.add_enc_algos(enc_algos_s2c_vec, &mut kim, false);
            }

            if let Some(mac_algos_c2s_vec) = kim_cfg.mac_algos_c2s() {
                self.add_mac_algos(mac_algos_c2s_vec, &mut kim, true);
            }

            if let Some(mac_algos_s2c_vec) = kim_cfg.mac_algos_s2c() {
                self.add_mac_algos(mac_algos_s2c_vec, &mut kim, false);
            }

            if let Some(comp_algos_c2s_vec) = kim_cfg.comp_algos_c2s() {
                self.add_comp_algos(comp_algos_c2s_vec, &mut kim, true);
            }

            if let Some(comp_algos_s2c_vec) = kim_cfg.comp_algos_s2c() {
                self.add_comp_algos(comp_algos_s2c_vec, &mut kim, false);
            }

            if let Some(langs_c2s_vec) = kim_cfg.langs_c2s() {
                let mut new_langs = Vec::new();
                for lang in langs_c2s_vec {
                    new_langs.push(lang.clone());
                }
                if !new_langs.is_empty() {
                    kim.langs_c2s(new_langs);
                }
            }

            if let Some(langs_s2c_vec) = kim_cfg.langs_s2c() {
                let mut new_langs = Vec::new();
                for lang in langs_s2c_vec {
                    new_langs.push(lang.clone());
                }
                if !new_langs.is_empty() {
                    kim.langs_s2c(new_langs);
                }
            }

            if let Some(first_kex_packet_follows) = kim_cfg.first_kex_packet_follows() {
                kim.first_kex_packet_follows(first_kex_packet_follows);
            }
        }

        kim
    }

    fn readable(&mut self, event_loop: &mut EventLoop<Gosh>) -> GoshResult<()> {
        match self.sock.try_read_buf(&mut self.rx) {
            Ok(None) => {}
            Ok(Some(r)) => {
                trace!(self.stdout, "readable", "bytes_read" => r);
                let mut bytes_read = Vec::with_capacity(r);
                bytes_read.extend_from_slice(self.rx.bytes());
                match self.state {
                    state::Connection::VersionExchange => {
                        try!(self.version_exchange(&bytes_read, r));
                        let id_bytes = self.id().to_string().into_bytes();
                        try!(self.write(event_loop, &id_bytes));
                    }
                    state::Connection::KeyExchangeInit => {
                        let payload = try!(packet::into_payload(bytes_read, None));

                        if let SshPayload::KeyExchangeInit(kex) = payload {
                            debug!(self.stdout, "readable", "kex" => format!("{}", kex));
                            self.state = self.state.next(ServerKeyExchangeReceived);
                        } else {
                            return Err(GoshErr::InvalidKexPayload);
                        }

                        let kex_init: kexinit::Message = self.build_kex_message();
                        let kex_payload = try!(kex_init.into_bytes());
                        let packet = try!(packet::as_bytes(kex_payload, None));
                        try!(self.write(event_loop, &packet));
                    }
                    _ => {
                        error!(
                            self.stderr,
                            "readable",
                            "error" => "Invalid Readable State",
                            "state" => format!("{}", self.state)
                        );
                        return Err(GoshErr::InvalidReadableState);
                    }
                }

                Buf::advance(&mut self.rx, r);
                self.interest.insert(EventSet::readable());
            }
            Err(e) => {
                error!(
                    self.stderr,
                    "readable",
                    "error" => "buf read error",
                    "detail" => format!("{}", e)
                );
                self.interest.remove(EventSet::readable());
            }
        };

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    fn reset_connection(&mut self, event_loop: &mut EventLoop<Gosh>, token: Token) {
        if self.token == token {
            trace!(self.stdout, "reset_connection", "event" => "loop shutdown");
            event_loop.shutdown();
        } else {
            warn!(self.stderr, "reset_connection", "event" => "unknown token", "token" => token.0);
        }
    }

    #[cfg_attr(feature="clippy", allow(use_debug))]
    pub fn write(&mut self, event_loop: &mut EventLoop<Gosh>, bytes: &[u8]) -> GoshResult<usize> {
        let count = self.tx.write_slice(bytes);
        trace!(self.stdout, "writable", "interest" => format!("{:?}", self.interest));
        self.interest.insert(EventSet::writable());
        try!(event_loop.reregister(&self.sock,
                                   self.token,
                                   self.interest,
                                   PollOpt::edge() | PollOpt::oneshot()));
        Ok(count)
    }

    #[cfg_attr(feature="clippy", allow(use_debug))]
    fn writable(&mut self, event_loop: &mut EventLoop<Gosh>) -> GoshResult<()> {
        trace!(self.stdout, "writable");

        match self.sock.try_write_buf(&mut self.tx) {
            Ok(None) => {
                trace!(self.stdout, "writable", "message" => "client flushing buf; WOULDBLOCK");
                self.interest.insert(EventSet::writable());
            }
            Ok(Some(r)) => {
                trace!(self.stdout, "writable", "bytes_written" => r);
                self.interest.insert(EventSet::readable());
                self.interest.remove(EventSet::writable());
            }
            Err(e) => {
                error!(
                    self.stderr,
                    "writable",
                    "error" => "buf write error",
                    "detail" => format!("{}", e)
                );
            }
        }

        trace!(self.stdout, "writable", "interest" => format!("{:?}", self.interest));

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    fn version_exchange(&mut self, bytes: &[u8], r: usize) -> GoshResult<()> {
        trace!(self.stdout, "version_exchange");
        // Parse any message here as a protocol version exchange message.  See
        // https://tools.ietf.org/html/rfc4253#section-4.2.
        // If the message is longer that 255 bytes or fails to match the message
        // format shutdown the connection
        let vep = String::from_utf8_lossy(bytes);
        if r < 256 && VER_EXCH_PROTO.is_match(&vep) {
            self.version_exchange.set_server_version(vep.into_owned());
            self.state = self.state.next(ServerVersionExchangeReceived);
            debug!(
                self.stdout,
                "version_exchange",
                "version_exchange" => format!("{}", self.version_exchange)
            );
            Ok(())
        } else {
            error!(
                self.stderr,
                "version_exchange",
                "error" => "Invalid Version Exchange",
                "version_exchange" => format!("{}", vep)
            );
            Err(GoshErr::InvalidVersionExchange)
        }
    }
}

impl Handler for Gosh {
    type Message = ();
    type Timeout = ();

    #[cfg_attr(feature="clippy", allow(use_debug))]
    fn ready(&mut self, event_loop: &mut EventLoop<Gosh>, token: Token, events: EventSet) {
        trace!(self.stdout, "ready", "events" => format!("{:?}", events));

        if events.is_hup() {
            trace!(self.stdout, "ready", "event" => "hup", "token" => token.0);
            self.reset_connection(event_loop, token);
            return;
        }

        if events.is_error() {
            error!(self.stderr, "ready", "event" => "error", "token" => token.0);
            self.reset_connection(event_loop, token);
            return;
        }

        if events.is_readable() {
            trace!(self.stdout, "ready", "event" => "readable", "token" => token.0);

            if let Err(e) = self.readable(event_loop) {
                error!(
                    self.stderr,
                    "ready",
                    "event" => "readable",
                    "error" => "conn_readable failed",
                    "token" => token.0,
                    "detail" => format!("{}", e)
                );
                self.reset_connection(event_loop, token);
            }
        }

        if events.is_writable() {
            trace!(self.stdout, "ready", "event" => "writable", "token" => token.0);

            if let Err(e) = self.writable(event_loop) {
                error!(
                    self.stderr,
                    "ready",
                    "event" => "writable",
                    "error" => "conn_writable failed",
                    "token" => token.0,
                    "detail" => format!("{}", e)
                );
                self.reset_connection(event_loop, token);
            }
        }
    }
}
