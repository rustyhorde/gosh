use STDOUT_SW;
use slog::drain::IntoLogger;
use std::fmt;

#[derive(Clone, Copy)]
pub enum Connection {
    VersionExchange,
    KeyExchangeInit,
    KeyExchange,
    Secured,
}

impl fmt::Display for Connection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Connection::*;

        let cs_str = match *self {
            VersionExchange => "VersionExchange",
            KeyExchangeInit => "KeyExchangeInit",
            KeyExchange => "KeyExchange",
            Secured => "Secured",
        };
        write!(f, "{}", cs_str)
    }
}

pub enum ConnectionEvent {
    ServerVersionExchangeReceived,
    #[allow(dead_code)]
    ServerKeyExchangeReceived,
    #[allow(dead_code)]
    NewKeys,
}

impl Connection {
    pub fn next(self, event: ConnectionEvent) -> Connection {
        use self::ConnectionEvent::*;
        use self::Connection::*;

        let next = match (self, event) {
            (VersionExchange, ServerVersionExchangeReceived) => KeyExchangeInit,
            (KeyExchangeInit, ServerKeyExchangeReceived) => KeyExchange,
            (KeyExchange, NewKeys) => Secured,
            (_, _) => self,
        };

        let stdout = STDOUT_SW.drain().into_logger(o!());
        trace!(
            stdout,
            "next",
            "event" => "connection state change",
            "current" => format!("{}", self),
            "next" => format!("{}", next)
        );
        next
    }
}
